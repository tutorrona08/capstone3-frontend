import BarChart from '../components/VerticalBar'
import PieChart from '../components/PieChart'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import numbersWithCommas from '../helpers/commas'
import toNum from '../helpers/toNum'

import {Fragment,useState,useEffect,useContext} from 'react'
import {DropdownButton,Dropdown,Container,Row,Col,Form,Button,Table} from 'react-bootstrap'
import Swal from 'sweetalert2'

import Router from 'next/router'

import UserContext from '../userContext'

export default function Profile(){

	const {user} = useContext(UserContext)

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [balance,setBalance] = useState("")

	const [categories,setCategories]=useState([])
	const [categoriesList,setCategoriesList] = useState([])

	const [recordDescription,setRecordDescription] = useState("")
	const [recordCategory,setRecordCategory] = useState("")
	const [recordType, setRecordType] = useState("")
	const [recordAmount, setRecordAmount] = useState("")

	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(recordAmount !=="" && recordCategory !=="" && recordType !=="" && recordDescription !==""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	},[recordType,recordAmount,recordDescription,recordCategory])		
	

	function addRecord(e){

		e.preventDefault()



		let balanceHolder = 0

		if(recordType == "Income"){
			balanceHolder = toNum(balance) + toNum(recordAmount)
		} else if(recordType == "Expense"){
			balanceHolder = toNum(balance) - toNum(recordAmount)
		}

		console.log(balanceHolder)

		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/transaction', {
			method: 'POST',
			headers: { 
				'Content-Type':'application/json',
				'Authorization':`Bearer ${(localStorage.getItem('token'))}`
			},
			body: JSON.stringify({
				id: user.id, 
				type: recordType,
				category: recordCategory,
				amount: recordAmount,
				description: recordDescription,
				balance: balanceHolder
			})
		})
		.then(res => res.json())
		.then(data => {

			fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/updateBalance', {
				method: 'PUT',
				headers: { 
					'Content-Type':'application/json',
					'Authorization':`Bearer ${(localStorage.getItem('token'))}`
				},
				body: JSON.stringify({
					id: user.id, 
					balance: balanceHolder
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				Router.reload()
			})	

		})	

	}		

	useEffect(()=>{


		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/details', {
			headers: {
				Authorization: `Bearer ${(localStorage.getItem('token'))}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setFirstName(data.firstName)
			setLastName(data.lastName)
			setBalance(numbersWithCommas(data.balance))
		})	
	},[])	


   	useEffect(()=>{

		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/allCategories', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setCategories(data)
	   		const categoryOptions = []
			data.forEach(cat => {				
				if(recordType === "Expense" && cat.type === "Expense"){
					categoryOptions.push(cat)
				} else if(recordType === "Income" && cat.type === "Income"){
					categoryOptions.push(cat)
				}

			})

			setCategoriesList(categoryOptions)
			setRecordCategory("")
		})   		


   	},[recordType])


   	useEffect(()=>{

   		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/allTransactions', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {

			data.reverse()
			console.log(data)
			
		})  
   	},[])   		



	const transactionTypes = [
		{
		    id: 1,
		    value: 'Income'
		}, {
		    id: 2,
		    value: 'Expense'
		}
	]

   	const transactions = transactionTypes.map(option => {
   		return(
            <option key={option.id} value={option.value}>                                   
            	{option.value}
			</option>
   		)
    })

    const categoryPlot = categoriesList.map(option => {
    	return(
	        <option key={option._id} value={option.name}>                                   
	           	{option.name}
			</option>
    	)
    })    	


	// console.log(type)
	return (
		<Fragment>
			<div>
			


						<div className={styles.card}>							
							<h2 className="text-center">Hi! {firstName} {lastName}</h2>
							<h3 className="text-center">Balance: Php {balance} </h3>
						</div>
			
				<Row>
					<Col md={4}>
					<div className={styles.card}>							
								<a href="./categories" className="col-md-3 offset-6 text-center">Add Category Here</a>
							</div>

						<div className={styles.card}>							
							<Form onSubmit={e => addRecord(e)}>
									<h5 className="text-center">Record New Transaction</h5>
								<Form.Group>
									<Form.Control as="select" value={recordType} onChange={e => setRecordType(e.target.value)}> 
										<option value="" disabled>- Select Transaction Type</option>
										{transactions}
									</Form.Control>
								</Form.Group>	
								<Form.Group>								
									<Form.Control as="select" value={recordCategory} onChange={e => setRecordCategory(e.target.value)}> 
										<option value="" disabled>- Select Category</option>
										{categoryPlot}
									</Form.Control>								
								</Form.Group>
								<Form.Group controlId="recordDescription">
									<Form.Control type="text" placeholder="Description" value={recordDescription} onChange={e => setRecordDescription(e.target.value)} required/>
								</Form.Group>
								<Form.Group>								
									<Form.Control type="number" placeholder="Amount" value={recordAmount} onChange={e => setRecordAmount(e.target.value)} required/> 						
								</Form.Group>							
								{
								isActive
								? <Button className="working-button col-md-3" type="submit" variant="primary">Record</Button>
								: <Button variant="primary" disabled>Record</Button>
								}
								
							</Form>

						</div>						
							
					</Col>
					<Col>
						<div className={styles.card}>						
							<PieChart />
						</div>						
					</Col>				
																			
				</Row>

				<Row>
					<Col>						
						<div className={styles.card}>						
							<BarChart />
						</div>
					</Col>
				
				</Row>
			</div>
		</Fragment>
	)
}

		

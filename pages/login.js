import {useState,useEffect,useContext} from 'react'
import {Container,Row,Col,Carousel,Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import styles from '../styles/Home.module.css'
import Router from 'next/router'

import UserContext from '../userContext'

import {GoogleLogin} from 'react-google-login'

export default function Login(){

	const {user,setUser} = useContext(UserContext)
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(email !=="" && password !==""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	},[email,password])	

	function authenticate(e){

		e.preventDefault()

		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: { 'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			if (data.accessToken){
				localStorage.setItem("token", data.accessToken)
				fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}` 
					}
				})
				.then(res => res.json())
				.then(data => {
					
					
					localStorage.setItem("id", data._id)
					localStorage.setItem("email", data.email)
				
					setUser({

						id: data._id, 
						email: data.email,

					})

					Swal.fire({
						icon: "success",
						title: "Successfully Logged In.",
						text: "Thank you for logging in."
					})


					Router.push("/")
				})
			} else {
				Swal.fire({

					icon: "error",
					title: "Unsuccessful Login",
					text: "User credentials are wrong"		
				})
			}

		})

		setEmail("")
		setPassword("")



	}

	function authenticateGoogleToken(response){
		//this is google's response with our tokenId to be used to authenticate our google login user


		console.log(response)
		/*For Email Sending

		Pass the accessToken from googlr to allow us the use of Google API to send email to the google login uset who logs on for the first time.
			
	
		*/
		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/verify-google-id-token',{

			method: 'POST',
			headers:{

				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				tokenId: response.tokenId,
				accessToken:response.accessToken

			})


		})
		.then(res => res.json())
		.then(data => {

			
			
			//We will show alerts to show if the user logged in properly or if there ar errors.
			if(typeof data.accessToken !== 'undefined'){

				//set the accessToken into our localStorage as token:
				localStorage.setItem('token', data.accessToken)

				//run a fetch request to get our user's details and update our global user state and save our user details into the localStorage
				fetch ('https://shrouded-badlands-54395.herokuapp.com/api/users/details',{

					headers: {

						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data=>{

						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)

						//after gettting the user's details, update the global user state:

						setUser({

							email: data.email,
							isAdmin: data.isAdmin
						})

						//fire a sweetalert to inform the user of successful login:
						Swal.fire({

							icon: 'success',
							title: 'Successful Login'
						})

						Router.push('/profile')

				})
			}else {

					//if data.accessToken is undefined, therefore, data contains a property called error instead.

					if (data.error === "google-auth-error"){

						Swal.fire({

							icon: 'error',
							title: 'Google Authentication Failed'

						})
					} else if(data.error === "login-type-error"){

						Swal.fire({

							icon: 'error',
							title: 'Login Failed',
							text: 'You may have registered through a different procedure'

						})

					}
			   }

		})
	}


	// function failed(response){
	// 	console.log(response)
	// }

	return (

		<Container>
			<Row>
				<Col>					
					<div className={styles.card}>				
					<Form onSubmit={e => authenticate(e)}>
						<h1 class="text-center">Login</h1>
						<Form.Group controlId="userEmail">
							<Form.Control type="email" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)} required>
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="userPassword">
							<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required>
							</Form.Control>
						</Form.Group>		
						{
							isActive
							? <Button variant="info" className="btn-block mb-1" type="submit">Log In</Button>
							: <Button variant="primary" className="btn-block mb-1" disabled>Log In</Button>
						}
					
						<GoogleLogin

							clientId="976117523656-qp7kfmst0e0afd20ilk35o5bvh3r4mcn.apps.googleusercontent.com"
							buttonText="Login Using Google" 
							onSuccess={authenticateGoogleToken} 
							onFailure={authenticateGoogleToken} 
							cookiePolicy={'single_host_origin'} 
							className="w-100 text-center my-2 d-flex justify-content-center" />
					</Form>
						<a href="./register" className="btn-block text-center"> Not yet registered? Sign Up </a>
					</div>
				</Col>
				
			</Row>
		</Container>

		)
}
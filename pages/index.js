import Head from 'next/head'
import {Fragment,useEffect, useContext} from 'react'
import styles from '../styles/Home.module.css'
import Router from 'next/router'

import UserContext from '../userContext'

export default function Home() {

    const {user} = useContext(UserContext)

    useEffect(()=>{

        if (user.email !== null){
            Router.push("/profile")
        } else {
            Router.push("/login")
        }

    },[])   




    return(
          <h1 className={styles.title}>Mooney Tracker</h1>
        )
}
    // <div className={styles.container}>
  
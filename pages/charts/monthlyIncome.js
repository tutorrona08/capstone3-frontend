import styles from '../../styles/Home.module.css'
import {useState,useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import {Row,Col,Container} from 'react-bootstrap'
import moment from 'moment'

export default function MonthlyIncome(){

	const [months,setMonths] = useState([])
	const [monthlyIncome,setMonthlyIncome] = useState([])

	const [allTransactions, setAllTransactions] = useState([])

	useEffect(()=>{

		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/allTransactions',{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllTransactions(data)
		})
	},[])

	useEffect(() => {

		if(allTransactions.length > 0){

			let tempMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			allTransactions.forEach(element => {

				if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){

					tempMonths.push(moment(element.date).format('MMMM'))

				}

			})

			const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)

				}

			})

			setMonths(tempMonths)

		}

	},[allTransactions])

	useEffect(() => {

		setMonthlyIncome(months.map(month => {

			let income = 0

			allTransactions.forEach(element => {

				if(moment(element.date).format("MMMM") === month && element.type === "Income"){

					income += parseInt(element.amount)

				}

			})
			return income

		}))

	},[months])


	const incomeData ={

		labels: months,
		datasets: [{

			label: "Annual Income",
			backgroundColor: "#99b3ff",
			borderColor: "white",
			borderWidth: 1,
			hoverBackgroundColor: "#99b3ff",
			hoverBorderColor: "#99b3ff",
			data: monthlyIncome

		}]

	}


	const options = {

		scales:{

			yAxes:[

				{

					ticks: {

						beginAtZero: true
					}

				}

			]

		}

	}

	return(
		<>
		<Container>	
			<Row className="mt-5">
				<Col className={styles.card}>
					<h2 className="text-center">Income</h2>
					<Bar data={incomeData} options={options} />
				</Col>
			</Row>
		</Container>
		</>
		)

}
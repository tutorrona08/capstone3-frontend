import styles from '../../styles/Home.module.css'
import {useState,useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import {Row,Col,Container} from 'react-bootstrap'
import moment from 'moment'

export default function MonthlyExpense(){

	const [months,setMonths] = useState([])

	const [monthlyExpenses,setMonthlyExpenses] = useState([])

	const [allTransactions, setAllTransactions] = useState([])

	useEffect(()=>{

		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/allTransactions',{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllTransactions(data)
		})
	},[])

	useEffect(() => {

		if(allTransactions.length > 0){

			let tempMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			allTransactions.forEach(element => {

				if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){

					tempMonths.push(moment(element.date).format('MMMM'))

				}

			})

			const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)

				}

			})

			setMonths(tempMonths)

		}

	},[allTransactions])


		useEffect(() => {

		setMonthlyExpenses(months.map(month => {

			let expense = 0

			allTransactions.forEach(element => {

				if(moment(element.date).format("MMMM") === month && element.type === "Expense"){

					expense += parseInt(element.amount)

				}

			})
			return expense

		}))

	},[months])


	const expenseData ={

		labels: months,
		datasets: [{

			label: "Annual Expenses",
			backgroundColor: "ccd9ff",
			borderColor: "white",
			borderWidth: 1,
			hoverBackgroundColor: "ccd9ff",
			hoverBorderColor: "ccd9ff",
			data: monthlyExpenses

		}]

	}

	const options = {

		scales:{

			yAxes:[

				{

					ticks: {

						beginAtZero: true
					}

				}

			]

		}

	}

	return(
		<>
		<Container>	
			<Row className="mt-5">
				<Col className={styles.card}>
					<h2 className="text-center">Expenses</h2>
					<Bar data={expenseData} options={options} />
				</Col>
			</Row>
		</Container>
		</>
		)

}
import {useState,useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import {Row,Col,Container} from 'react-bootstrap'
import moment from 'moment'

export default function BarChart(){

	const [months,setMonths] = useState([])
	const [monthlyIncome,setMonthlyIncome] = useState([])
	const [monthlyExpenses,setMonthlyExpenses] = useState([])

	const [allTransactions, setAllTransactions] = useState([])

	useEffect(()=>{

		fetch('https://shrouded-badlands-54395.herokuapp.com/api/users/allTransactions',{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllTransactions(data)
		})
	},[])

	useEffect(() => {

		if(allTransactions.length > 0){

			let tempMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			allTransactions.forEach(element => {

				if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){

					tempMonths.push(moment(element.date).format('MMMM'))

				}

			})

			const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)

				}

			})

			setMonths(tempMonths)

		}

	},[allTransactions])

	useEffect(() => {

		setMonthlyIncome(months.map(month => {

			let income = 0

			allTransactions.forEach(element => {

				if(moment(element.date).format("MMMM") === month && element.type === "Income"){

					income += parseInt(element.amount)

				}

			})
			return income

		}))

	},[months])

		useEffect(() => {

		setMonthlyExpenses(months.map(month => {

			let expense = 0

			allTransactions.forEach(element => {

				if(moment(element.date).format("MMMM") === month && element.type === "Expense"){

					expense += parseInt(element.amount)

				}

			})
			return expense

		}))

	},[months])


	const incomeData ={

		labels: months,
		datasets: [{

			label: "Income",
			backgroundColor: "#ff5d8f",
			borderColor: "white",
			borderWidth: 1,
			hoverBackgroundColor: "#ff5d8f",
			hoverBorderColor: "#ff5d8f",
			data: monthlyIncome

		}]

	}

	const expenseData ={

		labels: months,
		datasets: [{

			label: "Expenses",
			backgroundColor: "Black",
			borderColor: "white",
			borderWidth: 1,
			hoverBackgroundColor: "Black",
			hoverBorderColor: "Black",
			data: monthlyExpenses

		}]

	}

	const options = {

		scales:{

			yAxes:[

				{

					ticks: {

						beginAtZero: true
					}

				}

			]

		}

	}

	return(
		<>
		<Container >	
			<Row>
				<Col>
					<div>
						<h2 className="text-center">Income</h2>
						<Bar data={incomeData} options={options} />
					</div>
				</Col>
				<Col>
					<div>				
						<h2 className="text-center">Expenses</h2>
						<Bar data={expenseData} options={options} />
					</div>
				</Col>
			</Row>
		</Container>
		</>
		)

}
import {Fragment,useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import styles from '../styles/Home.module.css'

import Link from 'next/link'

import UserContext from '../userContext'

export default function Footer(){

return (
		<footer className={styles.footer}>
	        <a
	          href="https://www.linkedin.com/in/rona-marie-tutor-127bb418a/"
	          target="_blank"
	          rel="noopener noreferrer"
	        >
	          <h5>Rona Marie Tutor &copy; 2021 | Full Stack Web Developer </h5>
	        </a>
	    </footer>
	)
}
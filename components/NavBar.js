import {Fragment,useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import styles from '../styles/Home.module.css'

import Link from 'next/link'

import UserContext from '../userContext'

export default function NavBar(){

	const {user} = useContext(UserContext)

	return(

			<Navbar bg="transparent" expand="lg">
				<Link href="/">
					<a className="navbar-brand inline"><h1>Mooney Tracker!</h1></a>
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse>
					<Nav className="ml-auto">
					
					{
						user.email
						?
							<Fragment>			
								<Link href='/profile'>
									<a className="nav-link" role="button">Profile</a>
								</Link>													
								<Link href='/analytics'>
									<a className="nav-link" role="button">Analytics</a>
								</Link>																									
								<Link href='/transactions'>
									<a className="nav-link" role="button">Transactions</a>
								</Link>								
								<Link href='/categories'>
									<a className="nav-link" role="button">Categories</a>
								</Link>	
								<Link href='/logout'>
									<a className="nav-link" role="button">Log Out</a>
								</Link>
							</Fragment>
						:
						<>
						<Link href='/register'>
									<a className="nav-link" role="button">Register</a>
						</Link>	
						<Link href='/login'>
									<a className="nav-link" role="button">Login</a>
						</Link>	
						</>	
					}
					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)
}